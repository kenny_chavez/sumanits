/*
    Author: Kenny Chavez
    Versi�n: 0.0.1
    Description: Report of csv file
*/
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <math.h>
#include <stdlib.h>
// Terminamos de importar las librerias que vamos a utilizar

//Utilizamos el namespace necesario para abreviar un poco el codigo
using namespace std;

// Declaramos una estructura con los datos que necesitaremos en el futuro
struct Data{
    string nit = "";
    float amount = 0;
};

int main () {
    // Comenzamos declarando dos variables tipo string para guardar en una,
    //el nombre del archivo y en la siguiente las lineas que se vayan a recorrer
    string filename = "";
    string line;

    // Imprimimos en pantalla datos del alumno
    cout << "Universidad Mariano Galvez                 Asignatura: Programacion 1" << endl;
    cout << "Nombre: Kenny Omar Chavez Sin              Carne: 5190 16 902" << endl;
    cout << "\nFilename: ";
    // Pedimos y leemos el nombre del archivo
    cin >> filename;
    cout << "-----------------------------------------------------------------\n";

    // Declaramos una variable con el nombre del archivo que vamos a utilizar
    ifstream myfile (filename.c_str());
    // Creamos un puntero en donde se va a guardar la informacion que necesitamos.
    Data * allData;
    // Creamos un vector de tipo string para guardar todas las lineas que tiene el archivo
    vector<string> allLines;
    // Inicializamos un contador para las lineas
    int countLine = 0;
    // Inicializamos una variable float para guardar el total general de todas las transacciones.
    float total = 0;

    if (myfile.is_open()) // Abrimos el archivo y verificamos si es posible abrirlo
    {
        while ( getline (myfile,line) ) // Iniciamos un loop que estar� activo mientras hayan lineas en el archivo por leer
        {
            allLines.push_back(line); // Guaradmos cada linea en el vector anterior
            countLine += 1; // Sumamos el contador
        }

        int mainCount = 0; // Creamos otro contador que nos servira para asignar un espacio de memoria especifico en el puntero
        allData = new Data[countLine]; // Asignamos el tama�o del puntero

        for(std::vector<string>::iterator it = allLines.begin(); it != allLines.end(); ++it) { // Recorremos el vector de todas las lineas
            string newString; // Declaramos una variable string
            stringstream ss(*it); // Declaramos una variable de tipo stringstream para poder usar el split de getline
            int count2 = 0; // Declaaramos un nuevo contador

            while(getline(ss, newString, ',')){ // Hacemos la separaci�n por comas dentro de cada linea
                string split [2]; // Declaramos un array tipo string para improvisar un split normal de cualquier otro lenguaje
                split[count2] = newString; // Guardamos el nuevo string en la posicion nueva del contador

                if(count2 == 0){ // Verificamos que el contador sea 0
                    allData[mainCount].nit = split[0]; // Guardamos en la posici�n exacta del puntero la informacion que necesitamos, en este caso el nit.
                }

                if(count2 == 2){ // Verificamos que el contador sea 2
                    allData[mainCount].amount = atof(split[2].c_str()); // Guardamos en la posici�n exacta del puntero la informacion que necesitamos, en este caso el monto.
                    total += atof(split[2].c_str()); // Sumamos cada monto todos los montos existentes
                }

                count2 += 1; // Sumamos el contador secundario
            }

            mainCount += 1; // Sumamos el contador principal
        }

        // Mostramos una estructura ordenada para los datos recogidos
        cout << "No. NIT                      Total Vendido" << endl;
        int numero = 0; // Declaramos un nuevo contador
        for(int i = 0; i < countLine; i++){// Iniciamos un loop para recorrer el puntero
            float amount = 0; // Declaramos una variable de tipo float para sumar el monto, por nit
            string nit = allData[i].nit; // Guardamos el nit en la variable reci�n inicializada
            if(nit != ""){ // Verificamos que el nit no sea igual a nada
                for(int a = 0; a < countLine; a++){ // Declaramos otro loop para recorrer nuevamente el puntero
                    if(nit == allData[a].nit){ // Evaluamos si el nit que esta recorriendo, lo encontro en el loop anterior
                        amount = amount + allData[a].amount; // Si es as�, sumamos el monto
                        allData[a].nit = ""; // Vaciamos el nit para que esta informaci�n no vuelva a ser le�da en el futuro
                    }
                }
                numero += 1; // Sumamos la numeraci�n de cada nit
                cout << numero << "   " << nit << "                Q. " << amount << endl; // Mostramos la numeraci�n, el nit y el monto total por cada nit
            }
        }
        cout << "                             --------------" << endl;
        cout << "Sumatoria NITs:              Q. " << total; // Mostramos la sumatoria total

        //Eliminamos el uso de memoria
        delete [] allData; // Eliminamos el espacio en memoria que usamos para el puntero
        myfile.close(); // Cerramos el archivo para que el proceso concluya exitosamente
    }

    else cout << "Error: No se ha encontrado el archivo especificado"; // Se mostrar� esto si el archivo no existe o no e v�lido

    return 0;
}
